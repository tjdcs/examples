/** ////////////////////////////////////////////////////////////////////////////
* @file stringformat.cpp
*
* @copyright 2018 Tucker Downs. MIT License
//////////////////////////////////////////////////////////////////////////////*/

#include <string>
#include <iostream>
#include <stdio.h>
#include <chrono>
#include <memory>

int main(int argc, char* argv[]) {
    using namespace std;
    using namespace std::chrono;

    const int NUM_RUNS = 30000000;

    cout << "Running " << NUM_RUNS << " iterations.\n";

    auto running_time_str = 0;
    auto running_time_ctos = 0;
    auto running_time_char = 0;

    auto format = "The number is \t %d. Some more more text\n";
    for(int i = 0; i < NUM_RUNS; i++) {
        {
            auto t1 = high_resolution_clock::now();
            auto s1 = make_unique<string>(256, 0);
            auto n = snprintf(s1->data(), s1->size(), format, argc);
            s1->resize(n);
            auto t2 = high_resolution_clock::now();
            running_time_str += duration_cast<std::chrono::microseconds>( t2 - t1 ).count();
        }
        {
            auto t1 = high_resolution_clock::now();
            char buf[256];
            size_t n = snprintf(buf, 256, format, argc);
            auto s = make_unique<string>(buf, n);
            auto t2 = high_resolution_clock::now();
            running_time_ctos += duration_cast<std::chrono::microseconds>( t2 - t1 ).count();
        }
        {
            auto t1 = high_resolution_clock::now();
            char* buf = new char[256];
            size_t n = snprintf(buf, 256, format, argc);
            auto t2 = high_resolution_clock::now();
            delete[] buf;
            running_time_char += duration_cast<std::chrono::microseconds>( t2 - t1 ).count();
        }
    }

    cout << "string buffer w/ reszize was \t" << running_time_str << " microsecconds\n";
    cout << "char buffer w/ copy to str was \t" << running_time_ctos << " microsecconds\n";
    cout << "char buffer w/o copy was \t" << running_time_char << " microsecconds\n";

    return 0;
}
