// Type your code here, or load an example.
#include <array>
#include <memory>
#include <stdio.h>   
#include <stdlib.h> 

#include <string>
#include <iostream>
#include <chrono>

using namespace std;

unique_ptr<array<int, 10>> useCPP(const int num) {
    auto ptr = unique_ptr<array<int, 10>>{new array<int, 10>};
    for(int i = 0; i < ptr->size(); i++) {
        ptr->at(i) = num + i;
    }
    return ptr;
}

int* useC(const int num) {
    int* ptr = (int*)malloc(10*sizeof(int));
    for(int i = 0; i < 10; i++) {
        ptr[i] = num + i;
    }
    return ptr;
}

int main(int argc, char* argv[]) {
    using namespace std::chrono;

    const int NUM_RUNS = 100000000;

    std::cout << "Running " << NUM_RUNS << " iterations.\n";

    auto running_time_useCPP = 0;
    auto running_time_useC = 0;
    long out = 0;
    {
        auto t1 = high_resolution_clock::now();
        for(int i = 0; i < NUM_RUNS; i++) {
            unique_ptr<array<int, 10>> ptr = useCPP(5);
            for(int i = 0; i < ptr->size(); i++)
            {
                ptr->at(i) += 1;
                out += ptr->at(i);
            }
        }
        auto t2 = high_resolution_clock::now();
        running_time_useCPP += duration_cast<std::chrono::microseconds>( t2 - t1 ).count();
    }

    {
        auto t1 = high_resolution_clock::now();
        for(int i = 0; i < NUM_RUNS; i++) 
        {
            int* ptr = useC(5);
            for(int i = 0; i < 10; i++)
            {
                ptr[i] += 1;
                out += ptr[i];
            }
            free(ptr);
        }
        auto t2 = high_resolution_clock::now();
        running_time_useC += duration_cast<std::chrono::microseconds>( t2 - t1 ).count();
    }

    std::cout << "modern C++ time: \t" << running_time_useCPP << " microseconds\n";
    std::cout << "       C   time: \t" << running_time_useC   << " microseconds\n";

    return out;
}
